#!/usr/bin python
# -*- coding: utf-8 -*-

"""
16. 3Sum Closest
    Given an array nums of n integers and an integer target, find three integers in nums such that 
    the sum is closest to target. Return the sum of the three integers. You may assume that each 
    input would have exactly one solution.

Example:
    Given array nums = [-1, 2, 1, -4], and target = 1.
    The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
"""


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        length = len(nums)
        if length < 3:
            return False

        sum, result = 0, nums[0] + nums[1] + nums[2]
        for i in range(0, length - 2):
            for j in range(i + 1, length - 1):
                for k in range(j + 1, length):
                    sum = nums[i] + nums[j] + nums[k]
                    if abs(sum - target) < abs(result - target):
                        result = sum
        return result
