#!/usr/bin python
# -*- coding: utf-8 -*-

"""
5. Longest Palindromic Substring
    Given a string s, find the longest palindromic substring in s. You may 
    assume that the maximum length of s is 1000.

Example 1:
    Input: "babad"
    Output: "bab"
    Note: "aba" is also a valid answer.
Example 2:
    Input: "cbbd"
    Output: "bb"
"""


class Solution:
    def longestPalindrome(self, s: str) -> str:
        """Get the longest palindrome-substring in a given string.
        Args:
            s (str): A string not longer than 1000.
        Returns:
            str: The longest substring of `s` being a palindrome.
        Note:
            We maintain an n*n matrix to store whether
        """
