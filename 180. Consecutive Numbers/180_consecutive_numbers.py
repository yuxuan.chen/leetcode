#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Table: Logs
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| num         | varchar |
+-------------+---------+
id is the primary key for this table.
 
Write an SQL query to find all numbers that appear at least three 
times consecutively.
Return the result table in any order.
The query result format is in the following example:

Logs table:
+----+-----+
| Id | Num |
+----+-----+
| 1  | 1   |
| 2  | 1   |
| 3  | 1   |
| 4  | 2   |
| 5  | 1   |
| 6  | 2   |
| 7  | 2   |
+----+-----+

Result table:
+-----------------+
| ConsecutiveNums |
+-----------------+
| 1               |
+-----------------+
1 is the only number that appears consecutively for at least 
three times.
"""

import sqlite3

conn = sqlite3.connect("example.db")
curr = conn.cursor()

curr.execute(
    """
CREATE TABLE IF NOT EXISTS Logs (
    Id int PRIMARY KEY, 
    Num int)
"""
)

curr.execute(
    """
INSERT INTO Logs (
    Id, Num
) values 
    ('1', '1'), 
    ('2', '1'), 
    ('3', '1'), 
    ('4', '2'), 
    ('5', '1'), 
    ('6', '2'), 
    ('7', '2');
"""
)

curr.execute(
    """
SELECT DISTINCT Num as ConsecutiveNums
FROM Logs
WHERE (Id + 1, Num) IN (SELECT * From Logs) 
AND (Id + 2, Num) IN (SELECT * From Logs);
"""
)
print(curr.fetchall())
conn.close()
