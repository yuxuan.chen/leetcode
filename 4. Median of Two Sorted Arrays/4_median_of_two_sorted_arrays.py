#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
4. Median of Two Sorted Arrays
    There are two sorted arrays nums1 and nums2 of size m and n respectively.
    Find the median of the two sorted arrays. The overall run time complexity 
    should be O(log (m+n)).
    You may assume nums1 and nums2 cannot be both empty.

Example 1:
    nums1 = [1, 3]
    nums2 = [2]
    The median is 2.0
Example 2:
    nums1 = [1, 2]
    nums2 = [3, 4]
    The median is (2 + 3) / 2 = 2.5
"""


class Solution:
    def findMedianSortedArrays(self, nums1: list, nums2: list) -> float:
        """Get the median of union of two sorted arrays.\n
        Args:
            nums1: A list of m sorted numbers.
            nums2: A list of n sorted numbers.
        Returns:
            float: Median of all these m+n numbers.
        Raises:
            ValueError: when both lists are empty.
        """
        m, n = len(nums1), len(nums2)
        len_ = m + n  # the length of merged
        if len_ == 0:  # both lists are empty
            raise ValueError

        nums = nums1 + nums2
        nums.sort()
        return (
            nums[len_ // 2] / 1.0
            if len_ % 2 == 1
            else (nums[len_ // 2 - 1] + nums[len_ // 2]) / 2.0
        )


if __name__ == "__main__":
    examples = [([1, 3], [2]), ([1, 2], [3, 4])]
    for pair in examples:
        print(Solution().findMedianSortedArrays(*pair))
