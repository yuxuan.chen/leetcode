#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Write a SQL query to rank scores. If there is a tie between two scores, 
both should have the same ranking. Note that after a tie, the next 
ranking number should be the next consecutive integer value. In other 
words, there should be no "holes" between ranks.

+----+-------+
| Id | Score |
+----+-------+
| 1  | 3.50  |
| 2  | 3.65  |
| 3  | 4.00  |
| 4  | 3.85  |
| 5  | 4.00  |
| 6  | 3.65  |
+----+-------+
For example, given the above Scores table, your query should generate 
the following report (order by highest score):
+-------+---------+
| score | Rank    |
+-------+---------+
| 4.00  | 1       |
| 4.00  | 1       |
| 3.85  | 2       |
| 3.65  | 3       |
| 3.65  | 3       |
| 3.50  | 4       |
+-------+---------+
Important Note: For MySQL solutions, to escape reserved words used as 
column names, you can use an apostrophe before and after the keyword. 
For example `Rank`.
"""

import sqlite3

conn = sqlite3.connect("example.db")
curr = conn.cursor()

curr.execute(
    """
CREATE TABLE IF NOT EXISTS Scores (
    Id int PRIMARY KEY, 
    Score DECIMAL(3,2));
"""
)
curr.execute(
    """
INSERT INTO Scores (
    Id, Score
) values 
    ('1', '3.5'), 
    ('2', '3.65'), 
    ('3', '4.0'), 
    ('4', '3.85'), 
    ('5', '4.0'), 
    ('6', '3.65');
"""
)

curr.execute(
    """
SELECT Score AS score, DENSE_RANK() OVER (ORDER BY Score DESC) AS `Rank` 
FROM Scores;
"""
)
print(curr.fetchall())

conn.close()
