#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
6. ZigZag Conversion
    The string "PAYPALISHIRING" is written in a zigzag pattern on a 
    given number of rows like this: 
    (you may want to display this pattern in a fixed font for better legibility)
        P   A   H   N
        A P L S I I G
        Y   I   R
    And then read line by line: "PAHNAPLSIIGYIR"
    Write the code that will take a string and make this conversion 
    given a number of rows:
    string convert(string s, int numRows);
Example 1:
    Input: s = "PAYPALISHIRING", numRows = 3
    Output: "PAHNAPLSIIGYIR"
    Example 2:
    Input: s = "PAYPALISHIRING", numRows = 4
    Output: "PINALSIGYAHRPI"
    Explanation:
        P     I     N
        A   L S   I G
        Y A   H R
        P     I
"""


class Solution:
    def convert(self, s: str, numRows: int) -> str:
        """Rearrange a string into a zig-zag shape and print
        it in reading order.\n
        Args:
            s (str): A string of any size.
        Returns:
            str: The rearranged order of characters after zig-zag.
        Notes:
            For each row we have `O(#cycles)` operations and
            `#cycles = O(n/cycle) = O(n/O(numRows))`.
            Thus `T(n) = numRows * O(n/numRows) = O(n).`
        """
        if numRows <= 1:
            return s
        s_len = len(s)
        cycle = 2 * numRows - 2  # length of one cycle
        result = ""
        for row in range(numRows):
            i = 0
            while cycle * i + row < s_len:
                result += s[cycle * i + row]
                # only append when at the top/bottom of each vertical
                if row % (numRows - 1) != 0 and cycle * i + cycle - row < s_len:
                    result += s[cycle * i + cycle - row]
                i += 1
        return result


if __name__ == "__main__":
    s = "PAYPALISHIRING"
    print(Solution().convert(s, 3))
    print(Solution().convert(s, 4))
