

/*
6. ZigZag Conversion
    The string "PAYPALISHIRING" is written in a zigzag pattern on a 
    given number of rows like this: 
    (you may want to display this pattern in a fixed font for better legibility)
        P   A   H   N
        A P L S I I G
        Y   I   R
    And then read line by line: "PAHNAPLSIIGYIR"
    Write the code that will take a string and make this conversion 
    given a number of rows:
    string convert(string s, int numRows);
Example 1:
    Input: s = "PAYPALISHIRING", numRows = 3
    Output: "PAHNAPLSIIGYIR"
    Example 2:
    Input: s = "PAYPALISHIRING", numRows = 4
    Output: "PINALSIGYAHRPI"
    Explanation:
        P     I     N
        A   L S   I G
        Y A   H R
        P     I
*/

class Solution {
    public String convert(String s, int numRows) {
        if (numRows <= 1)
            return s;
        
        int cycle = 2 * numRows - 2;  // length of one cycle
        String result = "";
        for (int row = 0; row < numRows; row++) {
            int i = 0;
            while (cycle * i + row < s.length()) {
                result += s.charAt(cycle * i + row);
                // only append when at the top/bottom of each vertical
                if (row % (numRows - 1) != 0 
                    && cycle * i + cycle - row < s.length())
                    result += s.charAt(cycle * i + cycle - row);
                i += 1;
            }
        }
        return result;
    }
}


public class Main {
    public static void main(String[] args) {
        String s = "PAYPALISHIRING";
        System.out.println(new Solution().convert(s, 3));
        System.out.println(new Solution().convert(s, 4));
    }
}