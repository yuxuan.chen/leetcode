#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Write a SQL query to get the nth highest salary from the Employee table.
+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+

For example, given the above Employee table, the nth highest salary where 
n = 2 is 200. If there is no nth highest salary, then the query should 
return null.
+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| 200                    |
+------------------------+
"""

import sqlite3

conn = sqlite3.connect("example.db")
cur = conn.cursor()

# create table Employee
cur.execute(
    """
CREATE TABLE IF NOT EXISTS Employee (
    Id int PRIMARY KEY,
    Salary int);
"""
)

# add rows
cur.execute(
    """
INSERT INTO Employee (
    Id, Salary
) VALUES 
    (1, 100), 
    (2, 200), 
    (3, 300);
"""
)

# define function
cur.execute(
    """
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
    SET N = N-1;
    RETURN (
        SELECT IFNULL(
            (SELECT DISTINCT Salary 
            FROM Employee 
            ORDER BY Salary 
            DESC LIMIT 1 OFFSET N), 
            NULL) 
    );
END;
"""
)

cur.execute(
    """
SELECT * FROM getNthHighestSalary(2);
"""
)
print(cur.fetchall())
conn.close()
