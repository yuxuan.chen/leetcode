#include <iostream>
#include <vector>

using std::vector;

/*
2. Add Two Numbers
    You are given two non-empty linked lists representing two non-negative 
    integers. The digits are stored in reverse order and each of their nodes 
    contain a single digit.
    Add the two numbers and return it as a linked list.
    You may assume the two numbers do not contain any leading zero, except 
    the number 0 itself.

Example:
    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
    Explanation: 342 + 465 = 807.
*/

// definition for singly-linked list
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};


class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode head(-1), *curr = &head;
        int carry = 0;

        while (l1 || l2 || carry) {
            if (l1)
                carry += l1->val, l1 = l1->next;
            if (l2)
                carry += l2->val, l2 = l2->next;
            // update pointer curr
            curr->next = new ListNode(carry % 10);
            curr = curr->next;
            carry /= 10;
        }
        return head.next;
    }
};


ListNode* linked_list(vector<int> nums) {
    ListNode head(-1), *curr = &head;
    for(int num: nums) {
        curr->next = new ListNode(num);
        curr = curr->next;
    }
    return head.next;
}


void print_linked_list(ListNode* head) {
    ListNode* curr = head;
    while (curr != nullptr) {
        std::cout << curr->val;
        if (curr->next != nullptr)
            std::cout << " -> ";
        curr = curr->next;
    }
}


int main(void) {
    vector<int> v1{2, 4, 3}, v2{5, 6, 4};
    ListNode* l1 = linked_list(v1);
    ListNode* l2 = linked_list(v2);
    ListNode* result = Solution().addTwoNumbers(l1, l2);
    print_linked_list(result);
    return 0;
}