/*
2. Add Two Numbers
    You are given two non-empty linked lists representing two non-negative 
    integers. The digits are stored in reverse order and each of their nodes 
    contain a single digit.
    Add the two numbers and return it as a linked list.
    You may assume the two numbers do not contain any leading zero, except 
    the number 0 itself.

Example:
    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
    Explanation: 342 + 465 = 807.
*/


 // Definition for singly-linked list.
class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }


class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // initialize the result linked list
        ListNode head = new ListNode(-1);
        ListNode curr = head;
        int carry = 0;

        while (l1 != null || l2 != null || carry > 0) {
            if (l1 != null)
                carry += l1.val; l1 = l1.next;
            if (l2 != null)
                carry += l2.val; l2 = l2.next;
            curr.next = new ListNode(carry % 10);
            curr = curr.next;
            carry /= 10;
        }
        return head.next;
    }  
}


public class Main {
    // build a linked list from an array
    public static ListNode arrayToLinkedList(int[] nums) {
        ListNode head = new ListNode(-1);
        ListNode curr = head;
        for (int num: nums) {
            curr.next = new ListNode(num);
            curr = curr.next;
        }
        return head.next;
    }

    // print the values in order in a linked list given its head
    public static void printLinkedList(ListNode head) {
        ListNode curr = head;
        while (curr != null) {
            System.out.print(curr.val);
            if (curr.next != null)
                System.out.print(" -> ");
            else
                System.out.println("");
            curr = curr.next;
        }
    }

    public static void main(String[] args) {
        ListNode l1 = arrayToLinkedList(new int[]{2, 4, 3});
        ListNode l2 = arrayToLinkedList(new int[]{5, 6, 4});
        printLinkedList(new Solution().addTwoNumbers(l1, l2));
    }
}
