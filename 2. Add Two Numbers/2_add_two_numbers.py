#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
2. Add Two Numbers
    You are given two non-empty linked lists representing two non-negative 
    integers. The digits are stored in reverse order and each of their nodes 
    contain a single digit.
    Add the two numbers and return it as a linked list.
    You may assume the two numbers do not contain any leading zero, except 
    the number 0 itself.

Example:
    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
    Explanation: 342 + 465 = 807.
"""


class ListNode:
    """List Nodes of one data field.

    Args:
        x (int): a value from 0, 1, ..., 9 to represent a digit.
    """

    def __init__(self, x: int = 0):
        self.val = x
        self.next = None


class Solution(object):  # O(n)
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Add two integers in the form of linked lists.

        Given two linked lists `l1` and `l2`, each representing a
        positive integer from LSB to MSB. We do the add operation
        and give the result also in the form of a linked list.

        Args:
            l1 (ListNode): A list of 1-digits.
            l2 (ListNode): A list of 1-digits.
        Returns:
            ListNode: A list of 1-digits to give the sum.
        Note:
            We linearly traverse the two lists at the same pace.
            Thus time cost is `O(max{len(l1), len(l2)})`.
        """
        head = curr = ListNode(-1)
        carry = 0
        while l1 or l2 or carry:
            if l1:
                carry += l1.val
                l1 = l1.next
            if l2:
                carry += l2.val
                l2 = l2.next

            curr.next = ListNode(carry % 10)
            carry //= 10
            curr = curr.next
        return head.next


def linked_list(nums: list = [0]) -> ListNode:
    """Build a linked list from a list of 1-digits.

    Args:
        nums (list): a list of 1-digits.
    Returns:
        ListNode: The (real) head node of the linked list we built.
    """
    head = curr = ListNode(-1)
    for num in nums:
        curr.next = ListNode(num)
        curr = curr.next
    return head.next


def print_linked_list(head: ListNode = None):
    """Print a linked list starting from its head.

    Given a head node, we print all the values in order after it.
    For example, if the values can be listed as `[2, 4, 3]` in
    order, we print this linked list as `2 -> 4 -> 3`.

    Args:
        head (ListNode): The starting node of our print.
    """
    curr = head
    while curr is not None:
        print(curr.val, end="")
        if curr.next is not None:
            print(" -> ", end="")
        else:
            print("\n", end="")
        curr = curr.next


if __name__ == "__main__":
    l1 = linked_list([2, 4, 3])
    l2 = linked_list([5, 6, 4])
    result = Solution().addTwoNumbers(l1, l2)
    print_linked_list(result)
