#!/usr/bin python
# -*- coding: utf-8 -*-

"""
19. Remove Nth Node From End of List
    Given a linked list, remove the n-th node from the end of list and return its head.

Example:
    Given linked list: 1->2->3->4->5, and n = 2.
    After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:
    Given n will always be valid.
Follow up:
    Could you do this in one pass?
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        fast = slow = head
        for _ in range(n):
            fast = fast.next

        # delete the head pointer
        if fast is None:
            new_head = head.next
            del head
            return new_head

        while fast.next is not None:
            fast = fast.next
            slow = slow.next

        dummy = slow.next
        slow.next = dummy.next
        del dummy

        return head
