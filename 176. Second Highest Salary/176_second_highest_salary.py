#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Write a SQL query to get the second highest salary from the Employee table.

Table: Employee
+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+

For example, given the above Employee table, the query should return 200 as 
the second highest salary. If there is no second highest salary, then the query 
should return null.
+---------------------+
| SecondHighestSalary |
+---------------------+
| 200                 |
+---------------------+
"""

import sqlite3

conn = sqlite3.connect("example.db")
cur = conn.cursor()

# create table Employee
cur.execute(
    """
CREATE TABLE IF NOT EXISTS Employee (
    Id int PRIMARY KEY,
    Salary int);
"""
)

# add rows
cur.execute(
    """
INSERT INTO Employee (
    Id, Salary
) VALUES 
    (1, 100), 
    (2, 200), 
    (3, 300);
"""
)

cur.execute(
    """
SELECT 
    (SELECT DISTINCT Salary 
    FROM Employee 
    ORDER BY Salary 
    DESC LIMIT 1 OFFSET 1) 
As SecondHighestSalary;
"""
)
print(cur.fetchall())
conn.close()
