#include <iostream>
#include <string>
#include <algorithm>

using std::string;

/*
9. Palindrome Number
    Determine whether an integer is a palindrome. An integer is a 
    palindrome when it reads the same backward as forward.

Example 1:
    Input: 121
    Output: true
Example 2:
    Input: -121
    Output: false
    Explanation: From left to right, it reads -121. From right to left, 
    it becomes 121-. Therefore it is not a palindrome.
Example 3:
    Input: 10
    Output: false
    Explanation: Reads 01 from right to left. Therefore it is not 
    a palindrome.
Follow up:
    Coud you solve it without converting the integer to a string?
*/

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0)
            return false;
        
        string copy = std::to_string(x);
        string rev = copy;
        std::reverse(rev.begin(), rev.end());
        return (rev == copy);
    }
};


int main() {
    int nums[] = {121, -121, 10};
    for (const int &num : nums)
        std::cout << Solution().isPalindrome(num) << std::endl;
    return 0;
}