#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
12. Integer to Roman
    Roman numerals are represented by seven different symbols: 
    I, V, X, L, C, D and M.
        Symbol       Value
        I             1
        V             5
        X             10
        L             50
        C             100
        D             500
        M             1000
    For example, two is written as II in Roman numeral, just two 
    one's added together. 
    Twelve is written as, XII, which is simply X + II. The number 
    twenty seven is written as XXVII, 
    which is XX + V + II.
    Roman numerals are usually written largest to smallest from 
    left to right. However, the numeral for four is not IIII. Instead, 
    the number four is written as IV. Because the one is before the 
    five we subtract it making four. The same principle applies to the 
    number nine, which is written as IX. There are six instances where 
    subtraction is used:
    I can be placed before V (5) and X (10) to make 4 and 9. 
    X can be placed before L (50) and C (100) to make 40 and 90. 
    C can be placed before D (500) and M (1000) to make 400 and 900.
    Given an integer, convert it to a roman numeral. Input is guaranteed 
    to be within the range from 1 to 3999.

Example 1:
    Input: 3
    Output: "III"
Example 2:
    Input: 4
    Output: "IV"
Example 3:
    Input: 9
    Output: "IX"
Example 4:
    Input: 58
    Output: "LVIII"
    Explanation: L = 50, V = 5, III = 3.
Example 5:
    Input: 1994
    Output: "MCMXCIV"
    Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
"""


class Solution:
    def intToRoman(self, num: int) -> str:
        s = ""
        digits = []
        divisor = 1000

        while divisor >= 1:
            digits.append(num // divisor)
            num %= divisor
            divisor //= 10

        # thousand-digit
        s += "M" * digits[0]
        # hundred-digit
        if digits[1] in [4, 9]:
            s += "C" + "D" * int(digits[1] == 4) + "M" * int(digits[1] == 9)
        else:
            s += "D" * int(digits[1] >= 5) + "C" * (digits[1] % 5)
        # ten-digit
        if digits[2] in [4, 9]:
            s += "X" + "L" * int(digits[2] == 4) + "C" * int(digits[2] == 9)
        else:
            s += "L" * int(digits[2] >= 5) + "X" * (digits[2] % 5)
        # last-digit
        if digits[3] in [4, 9]:
            s += "I" + "V" * int(digits[3] == 4) + "X" * int(digits[3] == 9)
        else:
            s += "V" * int(digits[3] >= 5) + "I" * (digits[3] % 5)

        return s


if __name__ == "__main__":
    nums = [3, 4, 9, 58, 1994]
    for num in nums:
        print(Solution().intToRoman(num))
