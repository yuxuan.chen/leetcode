#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
7. Reverse Integer
    Given a 32-bit signed integer, reverse digits of an integer.

Example 1:
    Input: 123
    Output: 321
Example 2:
    Input: -123
    Output: -321
Example 3:
    Input: 120
    Output: 21
Note:
    Assume we are dealing with an environment which could only store 
    integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. 
    For the purpose of this problem, assume that your 
    function returns 0 when the reversed integer overflows.
"""


class Solution:
    def reverse(self, x: int) -> int:
        """Give a integer in reversed digit order.\n
        Args:
            x(int): An integer within [−2^31,  2^31 − 1].
        Returns:
            int: The integer with all the digits reversed, sign reserved.
        """
        sgn = (x > 0) - (x < 0)
        rev = int(str(x * sgn)[::-1])
        return sgn * rev * (rev < 2 ** 31)


if __name__ == "__main__":
    nums = [123, -123, 120, 1534236469]
    for num in nums:
        print(Solution().reverse(num))
