#include <iostream>

/*
7. Reverse Integer
    Given a 32-bit signed integer, reverse digits of an integer.

Example 1:
    Input: 123
    Output: 321
Example 2:
    Input: -123
    Output: -321
Example 3:
    Input: 120
    Output: 21
Note:
    Assume we are dealing with an environment which could only store 
    integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. 
    For the purpose of this problem, assume that your 
    function returns 0 when the reversed integer overflows.
*/

class Solution {
public:
    int reverse(int x) {
        long long result = 0;
        while (x) {
            result = 10 * result + x % 10;
            x /= 10;
            if (result < INT32_MIN || result > INT32_MAX)
                return 0;
        }
        return result;
    }
};


int main() {
    int nums[] = {123, -123, 120, 1534236469};
    for (const int &num : nums)
        std::cout << Solution().reverse(num) << std::endl;
    return 0;
}