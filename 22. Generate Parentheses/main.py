#!/usr/bin python
# -*- coding: utf-8 -*-

"""
22. Generate Parentheses
    Given n pairs of parentheses, write a function to generate all combinations of well-formed 
    parentheses.
    For example, given n = 3, a solution set is:
        [
        "((()))",
        "(()())",
        "(())()",
        "()(())",
        "()()()"
        ]
"""


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        """
        if n == 0:
            return []
        if n == 1:
            return ["()"]


        buff = self.generateParenthesis(n - 1)
        result = []
        for item in buff:
            result.append('(' + item + ')')
            result.append(item + '()')
            result.append('()' + item)
        result.pop()
        return result
        """

        def generate(p, left, right, parens=[]):
            if left:
                generate(p + "(", left - 1, right)
            if right > left:
                generate(p + ")", left, right - 1)
            if not right:
                parens += (p,)
            return parens

        return generate("", n, n)
