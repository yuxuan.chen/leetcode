#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
1. Two Sum
    Given an array of integers, return indices of the two numbers such that 
    they add up to a specific target.
    You may assume that each input would have exactly one solution, and you 
    may not use the same element twice.

Example:
    Given nums = [2, 7, 11, 15], target = 9,
    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].
"""


class Solution(object):
    def twoSum(self, nums: list, target: int) -> list:
        """Find two numbers in an array summing up to a specific number.

        Given an array `nums`, we try to get a pair of indices `[i,j]`, 
        such that the two values at these locations sum up exactly to 
        our `target`, i.e. `nums[i] + nums[j] = target`.

        Args:
            nums: A list of integers.
            target: A specified integer as the targeted sum.
        Returns:
            list: a pair of indices `[i, j]` such that 
                  `nums[i] + nums[j] = target`;                 
            Return False if no such pair exists.
        Raises:
            ValueError: If `nums` has less than 2 elements.
        Notes:
            For each item in the list, we need time O(logn) to search it in dict. \
            Thus `T(n) = O(nlogn), S(n) = O(n)`.
        """
        if len(nums) < 2:
            raise ValueError("There must be at least 2 elements.")

        buffer_dict = {}
        for i in range(len(nums)):
            if nums[i] in buffer_dict:
                return [buffer_dict[nums[i]], i]
            buffer_dict[target - nums[i]] = i
        return False

        """ O(n^2) solution
        for i in range(len(nums) - 1):
            num_to_find = target - nums[i]
            if num_to_find in nums[i+1:_len]:
                j = nums.index(num_to_find, i + 1, len(nums))
                return [i, j]
            i += 1
        return False
        """


if __name__ == "__main__":
    nums = [2, 7, 11, 15]
    target = 9
    print(Solution().twoSum(nums, target))
