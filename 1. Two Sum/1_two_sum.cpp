#include <iostream>
#include <vector>
#include <unordered_map>

using std::vector;

/*
1. Two Sum
    Given an array of integers, return indices of the two numbers such that 
    they add up to a specific target.
    You may assume that each input would have exactly one solution, and you 
    may not use the same element twice.

Example:
    Given nums = [2, 7, 11, 15], target = 9,
    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].
*/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::unordered_map<int, int> buffer_dict;

        for (int i = 0; i < nums.size(); ++i) {
            auto it = buffer_dict.find(target - nums[i]);
            if (it != buffer_dict.end())
                return vector<int> {it->second, i};           
            buffer_dict[nums[i]] = i;
        }
        // case of error
        return vector<int> {-1, -1};
    }
};


int main(void) {
    vector<int> nums{2, 7, 11, 15};
    int target = 9;
     
    vector<int> result = Solution().twoSum(nums, target);   
    if (result[0] == -1)
        return -1;
    std::cout << result[0] << ", " << result[1] << std::endl;
    return 0;
}