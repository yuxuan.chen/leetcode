/*
1. Two Sum
    Given an array of integers, return indices of the two numbers such that
    they add up to a specific target.
    You may assume that each input would have exactly one solution, and you
    may not use the same element twice.

Example:
    Given nums = [2, 7, 11, 15], target = 9,
    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].
*/

package main

import "fmt"

func twoSum(nums []int, target int) []int {
	bufferDict := make(map[int]int)
	for i, num := range nums {
		if _, ok := bufferDict[num]; ok {
			return []int{bufferDict[num], i}
		}
		bufferDict[target-num] = i
	}
	return nil
}

func main() {
	nums := []int{2, 7, 11, 15}
	target := 9
	fmt.Print(twoSum(nums, target))
}
