#!/usr/bin python
# -*- coding: utf-8 -*-

"""
11. Container With Most Water
    Given n non-negative integers a1, a2, ..., an , where each 
    represents a point at coordinate (i, ai). 
    n vertical lines are drawn such that the two endpoints of 
    line i is at (i, ai) and (i, 0). 
    Find two lines, which together with x-axis forms a container, 
    such that the container contains the most water.
Note: You may not slant the container and n is at least 2.

Example 1:
    Input: [1,8,6,2,5,4,8,3,7]
    Output: 49

Example 2:
    Input: height = [1,1]
    Output: 1

Example 3:
    Input: height = [4,3,2,1,4]
    Output: 16

Example 4:
    Input: height = [1,2,1]
    Output: 2
"""


class Solution:
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        max_area = cur_area = 0
        for i in range(len(height) - 1):
            for j in range(i + 1, len(height)):
                cur_area = (j - i) * min(height[i], height[j])
                max_area = max(max_area, cur_area)
        return max_area
