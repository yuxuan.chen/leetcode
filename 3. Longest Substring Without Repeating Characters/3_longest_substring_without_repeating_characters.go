/*
3. Longest Substring Without Repeating Characters
    Given a string, find the length of the longest substring
    without repeating characters.

Example 1:
    Input: "abcabcbb"
    Output: 3
    Explanation: The answer is "abc", with the length of 3.
Example 2:
    Input: "bbbbb"
    Output: 1
    Explanation: The answer is "b", with the length of 1.
Example 3:
    Input: "pwwkew"
    Output: 3
    Explanation: The answer is "wke", with the length of 3.
    Note that the answer must be a substring, "pwke" is a
    subsequence and not a substring.
*/

package main

import "fmt"

func lengthOfLongestSubstring(s string) int {
	if len(s) <= 1 {
		return len(s)
	}

	lastOccur := make(map[byte]int)
	maxLen := 1
	left := 0
	for right := 0; right < len(s); right++ {
		last, ok := lastOccur[s[right]] // try to find
		if ok && last >= left {
			left = last + 1
		} else {
			bufLen := right - left + 1
			if bufLen > maxLen {
				maxLen = bufLen
			}
		}
		lastOccur[s[right]] = right
	}
	return maxLen
}

func main() {
	examples := []string{"abcabcbb", "bbbbb", "pwwkew", ""}
	for _, s := range examples {
		fmt.Println(lengthOfLongestSubstring(s))
	}
}
