#include <iostream>
#include <string>
#include <unordered_map>

using std::string;

/*
3. Longest Substring Without Repeating Characters
    Given a string, find the length of the longest substring without repeating characters.

Example 1:
    Input: "abcabcbb"
    Output: 3 
    Explanation: The answer is "abc", with the length of 3. 
Example 2:
    Input: "bbbbb"
    Output: 1
    Explanation: The answer is "b", with the length of 1.
Example 3:
    Input: "pwwkew"
    Output: 3
    Explanation: The answer is "wke", with the length of 3. 
    Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if (s.size() <= 1)
            return s.size();
        
        std::unordered_map<char, int> last_occur;
        size_t max_len = 1;
        size_t left = 0;
        for (size_t right = 0; right < s.size(); ++right) {
            if (last_occur.find(s[right]) != last_occur.end()
                && last_occur[s[right]] >= left) {
                left = last_occur[s[right]] + 1;
            } else {
                size_t buf_len = right - left + 1;
                max_len = (buf_len > max_len) ? buf_len : max_len;
            }
            last_occur[s[right]] = right;             
        }
        return max_len;
    }
};

int main() {
    string examples[] = {"abcabcbb", "bbbbb", "pwwkew", ""};
    for (const string &s: examples)
        std::cout << Solution().lengthOfLongestSubstring(s) << std::endl;
    return 0;
}