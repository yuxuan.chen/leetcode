#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
3. Longest Substring Without Repeating Characters
    Given a string, find the length of the longest substring 
    without repeating characters.

Example 1:
    Input: "abcabcbb"
    Output: 3 
    Explanation: The answer is "abc", with the length of 3. 
Example 2:
    Input: "bbbbb"
    Output: 1
    Explanation: The answer is "b", with the length of 1.
Example 3:
    Input: "pwwkew"
    Output: 3
    Explanation: The answer is "wke", with the length of 3. 
    Note that the answer must be a substring, "pwke" is a subsequence 
    and not a substring.
"""


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:  # O(nlogn)
        """Get the length of the longest substring without repeating
        chracters.\n
        Args:
            s (str): A string which may contain duplicate characters.
        Returns:
            int: The greatest possible length of substring.
        Notes:
            For each item during our traversal, we need to search dict for it.
            Thus `T(n) = O(nlogn), S(n) = O(n)`.
        """
        if len(s) <= 1:
            return len(s)

        # a dictionary to record indices where letters last occur
        last_occur = {}
        max_len = 1
        left = 0
        for right in range(0, len(s)):
            if s[right] in last_occur and last_occur[s[right]] >= left:
                left = last_occur[s[right]] + 1
            else:
                max_len = max(max_len, right - left + 1)
            last_occur[s[right]] = right
        return max_len


if __name__ == "__main__":
    examples = ["abcabcbb", "bbbbb", "pwwkew", "", "tmmzuxt", "aabaab!bb"]
    for s in examples:
        print(Solution().lengthOfLongestSubstring(s))
