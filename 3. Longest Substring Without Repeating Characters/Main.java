import java.util.HashMap;
import java.util.Map;

/*
3. Longest Substring Without Repeating Characters
    Given a string, find the length of the longest substring 
    without repeating characters.

Example 1:
    Input: "abcabcbb"
    Output: 3 
    Explanation: The answer is "abc", with the length of 3. 
Example 2:
    Input: "bbbbb"
    Output: 1
    Explanation: The answer is "b", with the length of 1.
Example 3:
    Input: "pwwkew"
    Output: 3
    Explanation: The answer is "wke", with the length of 3. 
    Note that the answer must be a substring, "pwke" is a 
    subsequence and not a substring.
*/

class Solution {
    public int lengthOfLongestSubstring(String s) {
        if (s.length() <= 1)
            return s.length();

        Map<Character, Integer> lastOccur = new HashMap<Character, Integer>();
        int maxLen = 1;
        int left = 0;
        for (int right = 0; right < s.length(); ++right) {
            if (lastOccur.containsKey(s.charAt(right))
                && lastOccur.get(s.charAt(right)) >= left) {
                left = lastOccur.get(s.charAt(right)) + 1;
            } else {
                int bufLen = right - left + 1;
                maxLen = (bufLen > maxLen) ? bufLen : maxLen;
            }
            lastOccur.put(s.charAt(right), right);
        }
        return maxLen;
    }
}


public class Main {
    public static void main(String[] args) {
        String[] examples = {"abcabcbb", "bbbbb", "pwwkew", ""};
        for (String s: examples)
            System.out.println(
                new Solution().lengthOfLongestSubstring(s));
    }
}
