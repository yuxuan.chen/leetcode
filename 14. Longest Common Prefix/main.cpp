#include <iostream>
#include <vector>
#include <string>
using std::string;
using std::vector;

/*
 * 14. Longest Common Prefix
 * Write a function to find the longest common prefix string 
 * amongst an array of strings. 
 * If there is no common prefix, return an empty string "".
 *
 * Example 1:
 *   Input: ["flower","flow","flight"]
 *   Output: "fl"
 * Example 2:
 *   Input: ["dog","racecar","car"]
 *   Output: ""
 * Explanation: There is no common prefix among the input strings.
 * Note:
 *   All given inputs are in lowercase letters a-z.
*/


class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        if (strs.size() == 0)
            return string();
        if (strs.size() == 1)
            return strs[0];
        
        // get min_len to reduce time later
        size_t min_len = strs[0].length();
        for (const string &s: strs)
            if (s.length() < min_len)
                min_len = s.length();
        
        string result = string();
        for (size_t curr = 0; curr < min_len; ++curr) {
            for (size_t str_id = 1; str_id < strs.size(); ++str_id)
                if (strs[str_id][curr] != strs[0][curr])
                    return result;
            result += strs[0][curr];
        }
        return result;
    }
};

int main() {
    vector<vector<string>> examples = {
        {"flower", "flow", "flight"},
        {"dog", "racecar", "car"}
    };
    for (vector<string> &strs: examples)
        std::cout << Solution().longestCommonPrefix(strs) << std::endl;
    return 0;
}