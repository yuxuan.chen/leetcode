#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
14. Longest Common Prefix
    Write a function to find the longest common prefix string 
    amongst an array of strings.
    If there is no common prefix, return an empty string "".

Example 1:
    Input: ["flower","flow","flight"]
    Output: "fl"
Example 2:
    Input: ["dog","racecar","car"]
    Output: ""
    Explanation: There is no common prefix among the input strings.
Note:
    All given inputs are in lowercase letters a-z.
"""


class Solution:
    def longestCommonPrefix(self, strs: list) -> str:
        """Get the longest common prefix for a list of strings.\n
        Args:
            strs (list): A list of strings.
        Returns:
            str: The longest common prefix.
        Note:
            Time complexity is `O(min_len * #strs)`.
        """
        if len(strs) == 0:
            return ""
        if len(strs) == 1:
            return strs[0]

        # get minimum length of all the strings
        min_len = len(strs[0])
        for s in strs:
            min_len = min(min_len, len(s))

        result = ""
        for curr in range(min_len):
            for str_id in range(1, len(strs)):
                if strs[str_id][curr] != strs[0][curr]:
                    return result
            result += strs[0][curr]
        return result


if __name__ == "__main__":
    examples = [["flower", "flow", "flight"], ["dog", "racecar", "car"]]
    for example in examples:
        print(Solution().longestCommonPrefix(example))
