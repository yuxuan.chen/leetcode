#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Table: Person
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| PersonId    | int     |
| FirstName   | varchar |
| LastName    | varchar |
+-------------+---------+
PersonId is the primary key column for this table.

Table: Address
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| AddressId   | int     |
| PersonId    | int     |
| City        | varchar |
| State       | varchar |
+-------------+---------+
AddressId is the primary key column for this table.

Task:
Write a SQL query for a report that provides the following information 
for each person in the Person table, regardless if there is an address 
for each of those people:
    FirstName, LastName, City, State
"""

import sqlite3

conn = sqlite3.connect("example.db")
cur = conn.cursor()

# create table Person
cur.execute(
    """
CREATE TABLE IF NOT EXISTS Person (
    PersonID int PRIMARY KEY, 
    FirstName varchar, 
    LastName varchar);
"""
)

# create table Address
cur.execute(
    """
CREATE TABLE IF NOT EXISTS Address (
    AddressID int PRIMARY KEY, 
    PersonID int, 
    City varchar, 
    State varchar);
"""
)


# add rows to the two tables
cur.execute(
    """
INSERT INTO Person (
    PersonID, FirstName, LastName
) VALUES 
    (1, "Tim", "Davis"), 
    (2, "Jane", "Hopkins"), 
    (3, "Peter", "Jackson"), 
    (4, "Pablo", "Lucas");
"""
)
cur.execute(
    """
INSERT INTO Address (
    AddressID, PersonID, City, State
) VALUES 
    (1, 2, "London", "UK"), 
    (2, 4, "Madrid", "Spain"), 
    (3, 1, "New York", "US");
"""
)

# show tables
cur.execute(
    """
SELECT FirstName, LastName, City, State 
FROM Person 
LEFT JOIN Address 
ON Person.PersonID = Address.PersonID;
"""
)
print(cur.fetchall())
conn.close()
